package com.apex.memotouch;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.apex.iot.card.adapter.Adapter;
import com.apex.memotouch.utils.DeviceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *<p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 */
public class WifiSelectDialog extends Dialog {

    private ListView listView;
    private Context context;
    private List<Adapter> datas;
    private MyAdapter arrayAdapter;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public WifiSelectDialog(Context context, List<Adapter> datas) {
        super(context);
        this.context=context;
        this.datas=new ArrayList<>();
        this.datas.addAll(datas);
    }

    public WifiSelectDialog(Context context, int themeResId, List<Adapter> datas) {
        super(context, themeResId);
        this.context=context;
        this.datas=new ArrayList<>();
        this.datas.addAll(datas);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_connect_type_select);
        LinearLayout content= (LinearLayout) findViewById(R.id.dialog_connect_type_content);
        content.getLayoutParams().width=(int)(DeviceUtils.getScreenWidth(context)*0.75);
        listView= (ListView) findViewById(R.id.dialog_connect_type_list);
        arrayAdapter=new MyAdapter();
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (onItemClickListener!=null) onItemClickListener.onItemClick(datas.get(i));
                WifiSelectDialog.this.dismiss();
            }
        });
    }

    public void notifyDataChanged(){
        arrayAdapter.notifyDataSetChanged();
    }

    public void updateData(List<Adapter> adapterList) {
        if (this.datas!=null&& datas!=null) this.datas.clear();
        this.datas.addAll(adapterList);
        notifyDataChanged();
    }

    public interface OnItemClickListener{
        void onItemClick(Adapter wifiAdapter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK){
            this.dismiss();
            return true;
        }
        else return super.onKeyDown(keyCode, event);
    }

    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public Adapter getItem(int i) {
           return datas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView=new TextView(context);
            textView.setText(getItem(i).getName());
            textView.setHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, context.getResources().getDisplayMetrics()));
            textView.setGravity(Gravity.CENTER);
            return textView;
        }
    }
}
