package com.apex.memotouch;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apex.memotouch.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * 手机支持的读写连接方式Adapter
 *
 * <p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 */
public class WriteFilesAdapter extends BaseAdapter{
    private ArrayList<String> datas;

    public WriteFilesAdapter(ArrayList<String> datas){
        this.datas=datas;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public String getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_main_item,null);
        TextView textView= (TextView) view.findViewById(R.id.activity_main_item_tv);
        File f=new File(getItem(i));
        long size=0;
        if (f.exists()) size=f.length();
        textView.setText(getItem(i)+"   "+ StringUtils.generateFileSize(size));
        return view;
    }
}
