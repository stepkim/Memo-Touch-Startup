package com.apex.memotouch;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apex.memotouch.utils.StringUtils;

import java.util.ArrayList;

/**
 * 手机支持的读写连接方式Adapter
 *
 * <p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 *
 */
public class ReadFilesAdapter extends BaseAdapter {
    private ArrayList<ReadFile> readDatas;

    public ReadFilesAdapter(ArrayList<ReadFile> readDatas) {
        this.readDatas = readDatas;
    }

    @Override
    public int getCount() {
        return readDatas.size();
    }

    @Override
    public ReadFile getItem(int i) {
        return readDatas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            holder=new ViewHolder();
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_main_item, null);
            holder.textView = (TextView) view.findViewById(R.id.activity_main_item_tv);
            view.setTag(holder);
        }else {
            holder= (ViewHolder) view.getTag();
        }
        ReadFile readFile = getItem(i);
        holder.textView.setText(readFile.getPath() + "   " + StringUtils.generateFileSize(readFile.getLength()) + "   " + readFile.getProgress() + "%");
        return view;
    }



    class ViewHolder {
        TextView textView;
    }
}
