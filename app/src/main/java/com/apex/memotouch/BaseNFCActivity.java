package com.apex.memotouch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.apex.iot.card.Device;
import com.apex.memotouch.utils.NfcConfigure;


/**
 *
 * NFCActivity 基类
 *
 * <p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 *
 * Created by Simon
 */
public class BaseNFCActivity extends AppCompatActivity {

    /**
     * Configure NFC
     */
    protected NfcConfigure mNfcConfigure;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        windowManage();
        initialNFCForeground();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNfcConfigure.enableForegroundDispatch();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcConfigure.disableForegroundDispatch();
    }

    protected void showView(View view, boolean show){
        view.setVisibility(show? View.VISIBLE:View.GONE);
    }

    /**
     * keep screen on
     *
     * @author Jack
     */
    private void windowManage() {
        // keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // keep screen orientation portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * initial NFC foreground function
     *
     * @author Jack
     */
    private void initialNFCForeground() {
        mNfcConfigure = new NfcConfigure(this);
        mNfcConfigure.checkNfcFunction(); // NFC Check
    }

    protected void makeToastShort(int strId){
        makeToastShort(getString(strId));
    }

    protected void makeToastShort(String str){
        Toast.makeText(this,str,Toast.LENGTH_SHORT).show();
    }

    protected String getDeviceType(){
        SharedPreferences sp=getSharedPreferences("deviceType",MODE_PRIVATE);
        return sp.getString("deviceType", Device.TYPE_WIFI);
    }

    protected void saveDeviceType(String deviceType){
        SharedPreferences sp=getSharedPreferences("deviceType",MODE_PRIVATE);
        sp.edit().putString("deviceType",deviceType).commit();
    }
}
