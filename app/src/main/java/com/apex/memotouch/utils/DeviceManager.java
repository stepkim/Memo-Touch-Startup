package com.apex.memotouch.utils;

import android.util.Log;

import com.apex.iot.card.Device;
import com.apex.iot.card.DriverFactory;
import com.apex.iot.card.adapter.AdapterDevice;
import com.apex.iot.card.adapter.AdapterException;

/**
 * 加载底层驱动
 *<p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 *
 */
public class DeviceManager {
    private static DeviceManager instance;
    private Device device;
    private String TAG="DeviceManager";
    private String deviceType;

    private DeviceManager(){
        loadDriver();
    }

    public static DeviceManager getInstance(){
        if (instance==null){
            instance=new DeviceManager();
        }
        return instance;
    }

    private void loadDriver(){
        try {
            Class.forName("com.apex.iot.card.driver.MainDriverLoader");
            System.out.println("成功加载NFC设备驱动实现！");
        } catch (ClassNotFoundException e) {
            System.out.println("加载NFC设备驱动发生异常，请确保相关的类库已经导入项目程序中！");
            e.printStackTrace();
        }
    }

    public void setDeviceType(String deviceType){
        this.deviceType=deviceType;
        initDevice();
    }

    private void initDevice(){
        if (deviceType.equals(Device.TYPE_NFC)) {
            if (device!=null&& device instanceof AdapterDevice){  //切换为NFC 后,将AdapterDevice 中的Adapter 值空，清除底层轮询
                try {
                    ((AdapterDevice) device).setAdapter(null);
                } catch (AdapterException e) {
                    e.printStackTrace();
                }
            }
            Log.i(TAG,"---------TYPE_NFC--------");
            device= DriverFactory.getDevice(
                    Device.TYPE_NFC);
        }else if (deviceType.equals(Device.TYPE_WIFI)){
            Log.i(TAG,"---------TYPE_WIFI--------");
            device= DriverFactory.getDevice(
                    Device.TYPE_WIFI);
        }
    }

    public Device getDevice(){
        return device;
    }

}
